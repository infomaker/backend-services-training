# CCA: Tools Lab #
After this exercise, you should be able to convert documents to and from NavigaDoc and NewsML. 

[TOC]

## 1. Convert a NavigaDoc document to NewsML ##
In this exercise you will convert a NavigaDoc document to the corresponding NewsML format.

1. Get a document using [CCA Documents service](/cca/exercises/documents) and copy the `document` json value to the clipboard
2. In Postman, open the "Convert to NewsML" request
3. Paste the json of the fetched document as property `document` in the request payload (body), see example below
4. Execute the request
5. The response should be `200 OK` and in the response body you should have a `newsml` containing the converted (json escaped) NewsML representation of the document.

##### Example: Request #####
```json
{    
    "document": {
        "uuid": "REPLACE ME",
        "uri": "REPLACE ME",
       ...
    }
}
```

## 2. Convert a NewsML document to NavigaDoc ##
In this exercise you will convert a NewsML document to the corresponding NavigaDoc format.

1. Copy the (json escaped) NewsML returned from the "Convert a NavigaDoc document to NewsML" to the clipboard 
2. In Postman, open the "Convert to NavigaDoc" request
3. In the request payload, for `newsml`, paste the copied NewsML
4. Execute the request
5. The response should be `200 OK` and in the response body you should find the document in NavigaDoc format 