# CCA: Files Lab #
After completing these exercises, you should have knowledge of how CCA "file" endpoints work and how to use them. All referred requests in the exercises are found in the collection "CCA" in Postman.

CCA supports two ways of uploading an image, (1) a "direct" upload, i.e. all the bytes are uploaded using a AWS S3 pre-signed URL created by CCA and (2) a more "client oriented" upload that make use of [tus](https://tus.io/) (which among other things enables "resumable uploads") to upload files.

[TOC]

**Tips:**

* Keep the requests open in Postman after you finished an exercise. The responses from a request might be used in other exercises later on.

## 1. Upload an image ##
In this exercise you will upload a new image, all bytes at once.

### 1.1 Create upload for the image ###
1. In Postman, open the "Create upload" request
2. Execute the request
3. Response should be `200 OK` and in the response body you should have an `uploadId` and an `uploadUrl`.

### 1.2 Upload the image ###
1. In Postman, open the "Direct upload" request. Make sure "Authorization" is set to "No Auth"
2. In the request URL, replace the existing URL with the `uploadUrl` from previous request
3. In the request body, select "binary", and the file that you want to upload
4. Execute the request 
4. Response should be `200 OK`

The file has now been uploaded to an "upload" S3 bucket which in turn has started an asynchronous processing job (lambda) of the image. In order to know when the processing is done, you need to poll CCA for status of the upload using the `uploadId` from the first request.

### 1.3 Get upload status ###
**Note** that before you can start polling for status, the actual file upload (see previous request) must have finished (polling for status has to do with the processing of the uploaded file).  

1. In Postman, open the "Get upload status" request
2. In the request payload, set `uploadId` you got from the "Create upload for the image" request
3. Execute the request
4. Response should be `200 OK`
5. The response body contains a `status` indicating if the upload is finished or not. If `"status": "IN_PROGRESS"`, you need to keep polling until you get `"status": "DONE"` (or if something went wrong; `"status": "ERROR"`)

When the upload is finished, the response includes a "manifest" with data for the uploaded image.

* `uuid` - The v5 UUID to use when creating a document corresponding with the image (see "Create document for uploaded image" below)
* `hashedFilename` - The hashed name for the image 
* `uri` - The URI for the image (used to calculate the UUID)
* `contentClass` - Type of file
* `artifacts` - These are the different artifacts created as part of the uploading of the image

**Note!** CCA uses SHA-1 to hash the file and then base64-encodes the result and finally add the file extension in order to create the `hashedFilename`. The filename is then used to create a v5 UUID using the URI "im://image/{hashedFilename}" as the namespace identifier and name. The hashed filename and v5 UUID is used by CCA to identify duplicates when uploading files.

Next, as an optional exercise, get the "metadata" artifact that has been generated.

## 2. Get upload artifact (optional) ##
1. In Postman, open the "Get upload artifact" request
2. In the request payload, set `uploadId` to your uploadId (see above) and set `name` to the NavigaDoc file artifact. The name is found in the response of the "Get upload status" request, the artifact with `"type": "navigadoc"`, e.g. "hW5kiMRr8wjjmlQEjqDyx9csCOg.json"
3. Execute the request
4. The response should be `200 OK`

In the response you should see `content` property with a base64 encoded value. When decoded (an easy way to decode the string could be to use https://www.base64decode.org/), you should see a NavigaDoc (json) document which has been created by CCA.

So far, the image has only been uploaded to CCA. Next step is to create a corresponding metadata document for the image and store it in the repository.

## 3. Create document for uploaded image ##
**Note!** that you could use the artifact with type `navigadoc` to fetch a NavigaDoc document that CCA has created, including the metadata that is supported (and extracted from binary) to create the document for the image. In this exercise, you will create the document from scratch (using a template).

1. In Postman, open the "Create or Update a document" request
2. In the request payload, add `uploadId` using your uploadId (see above)
3. Copy the json from the [image template](/cca/templates/image.json) and paste it as the value for `document` in the request payload (body). The request body should look like the example below
4. Use the values from the "manifest" in the response from "Get upload status" request to set `uuid`, `uri` and property `filename` (maps to manifest `hashedFilename`)
5. In addition, in the template, there is a meta object with type `x-im/image` which holds the actual metadata for the image. The properties in the template are the core metadata fields used by CCA (and the Naviga's Content Creation Tools). Set these values for your image (you can use the artifact "metadata" (see above) to get part of the metadata, e.g. `width` and `height`)
5. Execute the request
6. The response should be `200 OK` and the created image document in the response body

##### Example: Request with uploadId and template as payload #####
```json
{
    "unit": "{{unit}}",
    "uploadId": "7d4b5ce4eaa7041229bcb97e01634058",
    "document": {
        "uuid": "REPLACE ME",
        "uri": "REPLACE ME",
       ...
    }
}
```

**Note!** You can set any metadata you like in the image's metadata document but remember that the metadata used should be known by consumers of the document, e.g. "Naviga Dashboard", "Naviga Writer", front-ends etc. 

If you look in the response document, you should be able to see the `created` and `updated` timestamps which have been added by CCA. 

In addition, there should be two links added, one with `"rel": "creator"` and one with `"rel": "updater"`. Apart from containing information of who has created and updated the document, these links are also bearers of Naviga ID organisation and unit. The organisation is extracted from the token you used authorized your request with, and the unit is the unit you specified in the request parameter `unit`.

## 4. Download image ##
In this exercise you will download the image that you have uploaded earlier.

### 4.1 Create download for the image ###
1. In Postman, open the "Create download" request
2. In the request payload, set the `UUID` to your image UUID
3. Execute the request
4. The response should be `200 OK { "downloadId": "5659ad46-e729-5650-8c52-12b6e6463ea7..." }`

CCA has now started, asynchronously, to process the image to be downloaded. This includes updating the binary with the original metadata as well as metadata updated using Naviga Content Creation Tools such as "Naviga Photo" and "Naviga Writer". In order to know when CCA has finished processing you need to poll for status.

### 4.2 Get download status ###
1. In Postman, open "Get download status" request
2. In the request payload, set the `downloadId` to the value you got from "Create download for the image" request above
3. Execute the request
4. The response should be `200 OK`. In the response body you should see a `status` indicating the status of the download. If `"status": "IN_PROGRESS"`, you need to keep polling until you get `"status": "DONE"` (or if something went wrong; `"status": "ERROR"`)

When download is finished the response will include a "manifest" with data for the download. The most interesting part should be the URL to the actual image. The URL is the value of the artifact with `"type": "image-url"`. This is a S3 presigned URL with a lifespan of 7 days. Copy (or click) the URL and paste it in your browser to download the image. 

## 5. Delete image ##
Time to clean up and delete your image.

1. In Postman, open the "Delete a document" request
2. In the request payload, update `UUID` with the UUID of your image
3. Execute the request
4. The response should be `200 OK {"deleted": true}`

## 6. Upload an image using TUS (optional) ##
In this exercise you will upload a new image using TUS.

### 6.1 Create upload for the image ###
1. In Postman, open the "Create TUS upload" request
2. Set headers `Upload-Length` to the size of the image you want to upload and verify that `Tus-Resumable` is set to "1.0.0"
3. Execute the request
5. Response should be `201 Created`

In the response header `location` you should see a long URL, copy this to your favorite text editor. You have now initiated the upload. Now it is time to start uploading the bytes.

### 6.2 Upload the image ###
1. In Postman, open the "Upload file" request (note that this is a PATCH, but we will send all the bytes at once)
2. In the URL for the request, replace the "REPLACE_ME" part of the URL with "path" of the URL you got in response header `location` in previous request (everything after `.../files/`)
3. In the "Body" tab, use "binary" and select your file as payload
4. Set request headers; `Upload-Length` to the size of the image you want to upload, `Tus-Resumable` to "1.0.0", `Upload-Offset` to 0 and `Content-Type` to "application/offset+octet-stream".
5. Execute the request
6. Response should be `204 No Content`

When the **whole file** has been uploaded, CCA starts to, asynchronously, process the image. In order to know when the processing is done, you need to poll CCA for status.

### 6.3 Get upload status ###
From this point on, the flow of request is the same as the "1.3 Get upload status" request above. The tricky part is to get hold of the `uploadId` which is found in the URL in the `location` response header, which was returned in the "6.1 Create upload for the image" request above. The upload id is hidden between the `.../files/` and the `+` sign (see example below). 

Given this URL:
```
https://cca-eu-west-1.saas-stage.infomaker.io/files/7d4b5ce4eaa7041229bcb97e01634058+XiLOusuVToLATVRO2LKG8kMYexSfd...
```
`uploadId` would be:
```
7d4b5ce4eaa7041229bcb97e01634058
```
