# CCA: Documents Lab #
After completing these exercises, you should have knowledge of how CCA "document" endpoints work and how to use them. All referred requests in the exercises are found in the collection "CCA" in Postman.

[TOC]

**Tips:**

* In some exercises you will be asked to supply a UUID. To create a valid v4 UUID in your shell, execute command `uuidgen`. Alternatively, you could fetch a UUID from https://www.uuidgenerator.net/.
* Keep the requests open in Postman after you finished an exercise. The responses from a request might be used in other exercises later on. 

## 1. Create new article ##
In this exercise you will create a new, minimal, article. 

1. In Postman, open the "Create or Update a document" request
2. Copy the json from the [article template](/cca/templates/article.json) and paste it as the value for `document` in the request payload (body). The request body should look like the example below
3. Replace all "REPLACE ME" with "your" values. Note that you need to create a valid UUID for `uuid` and `uri` (use the same UUID for both properties) in the document
4. Execute the request
5. Response should be `200 OK` and the created article should be in the `document` of the response. In addition, the response should include a `revision` and the `version` set to "1".

##### Example: Request with template as payload #####
```json
{
    "unit": "{{unit}}",
    "document": {
        "uuid": "REPLACE ME",
        "type": "x-im/article",
       ...
    }
}
```

If you look in the document in the response, you should be able to see the `created` and `updated` timestamps which have been added by CCA. 

In addition, there should be two links added, one with `"rel": "creator"` and one with `"rel": "updater"`. Apart from containing information of who has created and updated the document, these links are also bearers of Naviga ID organisation and unit. The organisation is extracted from the token you used authorized your request with, and the unit is the unit you specified in the request parameter `unit`.  

## 2. Check if article exists ##
In this exercise you will check if the article you just created actually exists

1. In Postman, open the "Check if a document exists" request
2. In the request payload, update `UUID` with the UUID you used when you created the article
3. Execute the request
4. You should get a `200 OK {"exists": true}` response

Next, test how CCA behaves when trying to get a document that does not exist. 

1. Update the `uuid` in the request payload with a **new** UUID
2. Execute the request
3. The response should be `200 OK {}` (this might look a bit odd, the "false" gets omitted because it's the zero value, and that's controlled by the protobuf JSON encoding)

## 3. Get article ##
So the article exists, time to get it.

1. In Postman, open the "Get a document" request
2. In the request payload, update `UUID` with the UUID for the created article (optional parameter; `version`)
3. Execute the request
4. CCA should return a `200 OK` and the article in `document` of the response. In addition, the response should include `revision`and `version` "1"

Note that you can fetch a specific version of a document by specifying `version` (integer) in the request payload.

Try getting a document that does not exist in CCA.

1. Update `UUID` with a **new** UUID
2. Execute the request
3. The response should be `404 Not Found {"code": "not_found", "msg": "not found"}`

## 4. Update article ##
Let's update the article with a few text paragraphs, and a youtube link.

1. In Postman, select the "Create or Update a document" tab (which you used in the first exercise)
2. Update the document in the request payload with a couple of paragraphs, and the [youtube link](/cca/templates/youtube.json). Note that these blocks should reside in the `content` property
3. Execute the request
4. The response should be `200 OK` with the updated article in `document` of the response. In addition, the response should include `revision`and `version` set to "2"

CCA supports "optimistic locking". By default, CCA does not impose locking but by supplying `lockingMode` and `expectedRevision` in the request payload, you can use locking.

1. In the request payload, add `lockingMode` and set value to `1`. Also add `expectedRevision` with a random string as value
2. Execute the request
3. The response should be `412 Precondition Failed`

Verify that you can update the article by supplying the current revision.

1. In Postman, select the "Get a document" request
2. In the request payload, update `UUID` with the UUID for you article
3. Execute the request
4. Copy the "revision" from `revision` in the response
5. Select the "Create or Update a document" again
6. Paste the revision into `expectedRevision` in the payload request 
7. Execute the request
8. The response should be `200 OK` with the updated article in `document` of the response 

## 5. Delete article ##
Time to clean up and delete your article.

1. In Postman, open the "Delete a document" request
2. In the request payload, update `UUID` with the UUID of your article
3. Execute the request
4. The response should be `200 OK {"deleted": true}`

What happens if you try to delete a document that does not exist?

1. Update `UUID` with a **new** UUID in the request payload
2. Execute the request
3. The response should be `404 Not Found {"code": "not_found", "msg": "not found"}`

## 6. Bad requests ##
Try out to see how CCA handle "bad requests". 

* Invalid json
* Missing `UUID` in request
* Etc.
 
