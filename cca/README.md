# ![](/images/cca.png) Content Creation API #
Content Creation API (CCA) is an API that is used by Naviga's Content Creation Tools, but it is also meant to be used 
by customers and their own solutions. Read the [CCA documentation](https://docs.infomaker.io/cca-api-documentation/) for more details.

## Prerequisite ##
In order to run the exercises you need: 

- to have a basic understanding of the [NavigaDoc json document](https://docs.infomaker.io/navigadoc/)
- to have done the exercises for [Naviga ID](/navigaid) (or have corresponding knowledge of Naviga ID)
- to download and setup Postman as described in the ["Prerequisite" for the Naviga ID lab](/navigaid/exercises/access-token). Note that Postman will not automatically request a new access token when the current one expires. When this happens, you need to fetch a new token as described in the link. 

## Exercises ##
1. [Documents](exercises/documents)
2. [Files](exercises/files)
3. [Tools](exercises/tools)
4. [NewsML documents](exercises/newsml) 

Note that there are a couple of ["known issues"](https://docs.infomaker.io/cca-api-documentation/known-issues) that might be good to be aware of.