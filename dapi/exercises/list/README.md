# DAPI: List execersise

**Tips:**

* In some exercises you will be asked to supply a UUID. To create a valid v4 UUID in your shell, execute command `uuidgen`. Alternatively, you could fetch a UUID from https://www.uuidgenerator.net/.
* Keep the requests open in Postman after you finished an exercise. The responses from a request might be used in other exercises later on. 

Note: In the first three steps we will be using CCA to create documents that will be used by DAPI.

## 1. Create new channel

1. In postman, open "Create a channel" request
2. Copy the json from [channel template](/dapi/templates/channel.json) and paste it as the value for document in the request payload (body).
3. Replace all "REPLACE ME" (See tip above on how to create a UUID)
4. Execute the request
5. Response should be 200 OK and the created article should be in the document of the response

## 2. Create a autor

1. In postman, open "Create an author" request
2. Copy the json from [auhtor template](/dapi/templates/author.json) and paste it as the value for document in the request payload (body).
3. Replace all "REPLACE ME" (See tip above on how to create a UUID)
4. Execute the request
5. Response should be 200 OK and the created article should be in the document of the response

## 3. Create an article
1. In postman, open "Create an article" request
2. Copy the json from [article template](/dapi/templates/article.json) and paste it as the value for document in the request payload (body).
3. Replace all "REPLACE ME" (See tip above on how to create a UUID) 
   The UUID:s for the channel and author (in links section) should be replaced with UUID:s previously created for channel and author.
4. Execute the request
5. Response should be 200 OK and the created article should be in the document of the response

Worth mentioning is that status of this article is `usable` which means it's published.


## 4. Get the article

Here is the first step where we acutally is using DAPI.

The first step is to get the article you created in step 3.

1. In postman, open "Get the Article".
2. In the URL, change ARTICLE_UUID to the uuid of the article you previously created.
3. Execute the request
4. The response should be the article. If you also look at the response headers you should also see the `version` of the article.

### 4a. Get a specified version
1. In postman, open "Get the Article version".
2. In the URL, change ARTICLE_UUID and VERSION (See 4.4 to see the latest version of an article).
3. Execute the request
4. The response should be the article. 

## 5. Get the list

1. In postman, open "Get the list".
2. In the URL change CHANNEL and AUTHOR_UUID to the id:s you've created in step 1 and 2.
3. Execute the request
4. Now should see a list containing your article

```json
{
    "region": "eu-west-1",
    "organisation": "tk",
    "channel": "CHANNEL_UUID",
    "name": "auto_author_AUTHOR_UUID",
    "title": "Auto generated list for channel CHANNEL_UUUID and link x-im/author",
    "capacity": 10,
    "items": [
        {
            "uuid": "ARTICLE_UUID",
            "type": "x-im/article",
            "version": "1",
            "score": 1592377335
        }
    ]
}
```


