# ![](/images/dapi.png) Distribution API #

Distribution API (DAPI) is a read API.

DAPI creates lists from articles,  DAPI analyzes all articles that are published (`usable`), then extract channels and concepts from the article and creates a list for every channel combined with the concept.

One example would be to get the authors `John Does` latest articles for a specific channel.

## Prerequisite ##
In order to run the exercises you need: 

- to have a basic understanding of the [NavigaDoc json document](https://docs.infomaker.io/navigadoc/)
- to have done the exercises for [Naviga ID](/navigaid) (or have corresponding knowledge of Naviga ID)
- to download and setup Postman as described in the ["Prerequisite" for the Naviga ID lab](/navigaid/exercises/access-token). Note that Postman will not automatically request a new access token when the current one expires. When this happens, you need to fetch a new token as described in the link. 

## Templates

We have prepared some templates for you that might come handy when doing these exercises.

You will find them under `templates` folder

## Exercises ##
1. [Lists](exercises/list)

