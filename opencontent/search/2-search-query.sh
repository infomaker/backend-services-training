#!/usr/bin/env bash

source settings
source get-token.sh false


QUERY="*:*"
START=0
LIMIT=1
SORT_INDEXFIELD=uuid
ASCENDING=true

SEARCH_RESPONSE=$(curl --silent --location --request GET "${OPENCONTENT_URL}/search?q=${QUERY}&start=${START}&limit=${LIMIT}&sort.indexfield=${SORT_INDEXFIELD}&sort.${SORT_INDEXFIELD}.ascending=${ASCENDING}" --header 'Accept: application/json' --header "Authorization: Bearer ${NAVIGA_ACCESS_TOKEN}")

echo ${SEARCH_RESPONSE} | jq .


