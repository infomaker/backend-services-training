#!/usr/bin/env bash

source settings
source get-token.sh false


QUERY="ConceptUuids:*"
CONTENTTYPE=Article
START=0
LIMIT=1

SORT_INDEXFIELD=uuid
ASCENDING=false

PROPERTIES="uuid,Headline,ConceptRelations[uuid,ConceptImType,ConceptName]"

# urlencoding of query
# space to +
PROPERTIES=${PROPERTIES// /+}

# # to %23
PROPERTIES=${PROPERTIES//#/%23}

# | to %7C
PROPERTIES=${PROPERTIES//|/%7C}

# [ to %5B
PROPERTIES=${PROPERTIES//[/%5B}

# ] to %5D
PROPERTIES=${PROPERTIES//]/%5D}

FILTERS="ConceptRelations(start=0 | limit=10|q=-ConceptImType:author)"

# space to +
FILTERS=${FILTERS// /+}

# | to %7C
FILTERS=${FILTERS//|/%7C}

SEARCH_RESPONSE=$(curl -s --data-urlencode --location --request GET "${OPENCONTENT_URL}/search?contenttype=${CONTENTTYPE}&q=${QUERY}&start=${START}&limit=${LIMIT}&sort.indexfield=${SORT_INDEXFIELD}&sort.${SORT_INDEXFIELD}.ascending=${ASCENDING}&properties=${PROPERTIES}&filters=${FILTERS}" --header 'Accept: application/json' --header "Authorization: Bearer ${NAVIGA_ACCESS_TOKEN}")

echo ${SEARCH_RESPONSE} | jq .