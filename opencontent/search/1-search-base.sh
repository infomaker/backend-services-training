#!/usr/bin/env bash

source settings
source get-token.sh false

SEARCH_RESPONSE=$(curl --silent --location --request GET "${OPENCONTENT_URL}/search?" --header 'Accept: application/json' --header "Authorization: Bearer ${NAVIGA_ACCESS_TOKEN}")

echo ${SEARCH_RESPONSE} | jq .


