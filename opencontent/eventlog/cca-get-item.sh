#!/usr/bin/env bash

UUID="${1}"
Version=${2}

curl -s --location --request POST "https://cca-eu-west-1.saas-prod.infomaker.io/twirp/cca.Documents/GetDocument" \
--header "Authorization: Bearer ${NAVIGA_ACCESS_TOKEN}" \
--header "Content-Type: application/json" \
--data-raw "{
    \"UUID\": \"${UUID}\",
    "\"version\"": ${Version}
}" | jq .