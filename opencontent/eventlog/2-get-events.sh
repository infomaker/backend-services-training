#!/bin/bash

source settings

source 1-get-token.sh false

echo "${OPENCONTENT_URL}/eventlog"

curl --silent --location --request GET "${OPENCONTENT_URL}/eventlog" --header 'Accept: application/json' --header "Authorization: Bearer ${NAVIGA_ACCESS_TOKEN}" | jq .