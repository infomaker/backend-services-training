#!/bin/bash

source settings
source 1-get-token.sh false

WAIT_TIME=1

function handleEvent() {

  events=${1}

  # incoming event structure
  # echo ${events} | jq .

  for row in $(echo "${events}" | jq -r '.events[] | @base64'); do
    # get the fields to print
    id=$(echo ${row} | base64 --decode | jq .id)
    eventType=$(echo ${row} | base64 --decode | jq -r .eventType)
    uuid=$(echo ${row} | base64 --decode | jq -r .uuid)
    contentType=$(echo ${row} | base64 --decode | jq -r .content.contentType)
    version=$(echo ${row} | base64 --decode | jq -r .content.version)

    # In this example processing is to print te row below
    echo "${id}  ${eventType} ${uuid} ${contentType}"

    # get object details from CCA if a non DELETE event
    if [ "${eventType}" != "DELETE" ];then
      ./cca-get-item.sh ${uuid} ${version}
    fi

    # save last processed event
    echo ${id} > lastevent
  done
}

echo "Last event $(cat lastevent) read from lastevent"

# uncomment to simulate invalid token from beginning
# NAVIGA_ACCESS_TOKEN="${NAVIGA_ACCESS_TOKEN}2"

while [ true ]; do
  lastEvent=$(cat lastevent)

  # echo "Last event ${lastEvent}"

  events=$(curl --silent --location --request GET "${OPENCONTENT_URL}/eventlog?event=${lastEvent}" --header 'Accept: application/json' --header "Authorization: Bearer ${NAVIGA_ACCESS_TOKEN}")

  UNAUTHORIZED=$(echo ${events} | grep -o "Unauthorized")

  if [ "${UNAUTHORIZED}" == "Unauthorized" ];then
    echo "Token timed out, need to refresh"
    # source 1-get-token.sh false
    numberOfEvents=-1
  else
    # get the number of events fetched
    numberOfEvents=$(echo ${events} | jq '.events | length')
  fi

  if [ ${numberOfEvents} -gt 0 ]; then
    handleEvent "${events}"
  elif [ ${numberOfEvents} -lt 0 ];then
    source 1-get-token.sh false
  else
    # only wait if nothing fetched
    echo "Will wait ${WAIT_TIME} seconds"
    sleep ${WAIT_TIME}
  fi
done
