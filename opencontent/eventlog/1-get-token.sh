#!/bin/bash

VERBOSE=true
if [ "${1}" != "" ]; then
  VERBOSE=${1}
fi

source settings

export NAVIGA_ACCESS_TOKEN=$(curl --silent --data "grant_type=client_credentials&client_id=$CLIENT_ID&client_secret=$CLIENT_SECRET" ${NAVIGA_ID_URL} | jq -r '.access_token')

if [ ${VERBOSE} == "true" ];then
  echo NAVIGA_ACCESS_TOKEN : ${NAVIGA_ACCESS_TOKEN}
fi
