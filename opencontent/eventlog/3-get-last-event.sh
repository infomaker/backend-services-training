#!/bin/bash

source settings
source 1-get-token.sh false

last_event_id=$(curl --silent --location --request GET "${OPENCONTENT_URL}/eventlog?event=-1" --header 'Accept: application/json' --header "Authorization: Bearer ${NAVIGA_ACCESS_TOKEN}" | jq .events[0].id)

echo ${last_event_id} > lastevent

echo "Last event is ${last_event_id} saved to file lastevent"



