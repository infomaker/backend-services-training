# Backend Services Training #

This repository serves as a placeholder for miscellaneous "training resources" for Naviga backend services such as
Open Content, CCA, Naviga ID etc.

**Note** that there are two environments, _stage_ and _prod_ (production), for the backend services. Perhaps it is redundant to
point out that stage is the environment that should be used for these exercises.

### Structure ###
Each backend service is represented by a folder, e.g. `/cca`, under which the training material for that particular service is found.

1. [Naviga ID](navigaid)
2. [CCA](cca)
3. [Open Content](opencontent)
4. [DAPI](dapi)

The idea is to do these labs in the order described above since they, to some extent, depend on each other.