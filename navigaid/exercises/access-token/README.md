# Naviga ID: Access Token and Client Credentials Lab

The purpose of these exercises is to make you feel comfortable using Naviga ID Access Tokens and Client Credentials. The first section is about retrieving access tokens, and the second part is about using them to access Naviga Content services such as CCA and OC.

All documentation regarding Naviga ID is available at https://docs.infomaker.io/navigaid.

[TOC]


## 1 Fetch access token using ID-token


### 1.1 Prerequisites

From your instructor, you'll your organization's name, `org`.

**Note** Fetching an access-token as a user requires you to have an account in the organization's Identity Provider. If you don't have that, skip this exercise and go to the next.

### 1.2 Manually fetch an access token using your ID-token in the browser

The documentation for the APIs used in this section can be found at https://docs.infomaker.io/navigaid/access-token/using-access-tokens#as-a-logged-in-user-using-id-token.

**Note** that the URL for fetching an access token includes environment qualifier ("stage"). When used in production (as opposed to this lab), this qualifier should be left out from the URL.

1. In your browser, go to https://demo.stage.imid.infomaker.io
2. Log in to your organization by entering the name in the top right corner, click "Login with org" and follow the instruction. Once successfully logged in, you will be returned the demo service page.
3. Open the "Developer tools" in your browser go to the console
4. Enter the following command `await fetch('https://access-token.stage.imid.infomaker.io/v1/token', { method: 'POST', credentials: 'include' })`. The `credentials: 'include'` config ensures your ID-token cookie will be included in the request.
5. Open the "Network" tab and inspect the request. In the response you'll find your newly created access_token.

Once you have an access token, using it is the same whether it orginated from an ID-token or Client Credentials.

## 2 Fetch and use an access token using Client Credentials

### 2.1 Prerequisites

#### Information from lab instructor

From your instructor, you'll need a a few values to be able to complete this lab and to continue with labs for other services. You'll need  `client_id`,  `client_secret` as well as your organization's name (`org`) in Naviga ID and `unit`.

#### Install Postman

The tooling used in these labs is Postman, a free and open source application that makes it easy to execute and inspect HTTP requests. Postman can be downloaded from https://www.postman.com.

#### Import the Naviga Content Collection into Postman

Download and import the [Naviga Content Postman Collection](../../../Naviga%20Content%20Lab.postman_collection.json) into Postman. Click the "Import" button in the top left corner in Postman and follow the instructions.

#### Update the Naviga Content Collection environment variables

After the import is complete, you'll need to update some environment variable values that are shared between the requests.

1. Click on the "●●●" next to the Naviga Content Lab collection in Postman
2. Click "Edit"
3. Select the "Variables" tab
4. In the "CURRENT VALUE" column, update all variables wih the value "REPLACE ME" with values you've been given by the instructor.

In the requests you'll encounter in these exercises, you will see `{{exampleEnvName}}` values in different places. They are placeholders for environment variables and will be replaced by Postman with the values you've just entered when the request is executed. In most cases (except in request body), you can inspect them by hovering your mouse over them.

### 2.2 Manually fetch an access token using Client Credentials

The documentation for the APIs used in this section can be found at https://docs.infomaker.io/navigaid/access-token/using-access-tokens#as-an-application-using-client-credentials

1. Go back to Postman
2. Under the "Naviga Content Lab" collection, in the "Naviga ID" folder, open the “Fetch access-token using Client Credentials” request
3. Click "Body" to check out the payload
4. Execute the request by clicking “Send”
5. Copy the `access_token` field value in the returned JSON. If you get an error, double check that you've entered the corret values for `client_id` and `client_secret` enviroment variables in the previous section.

### 2.3 Use a manually generated access token to access a Naviga Demo service

The documentation on how to use access-tokens can be found at https://docs.infomaker.io/navigaid/access-token/using-access-tokens#authenticate-using-an-access-token

1. Open Postman
2. Open the “Demo Test with manual access-token” request ("Naviga Content Lab" collection and "Naviga ID" folder)
3. Under Authorization, update the "Token" value with the `access_token` value from the previous section.
4. Execute the request by clicking “Send”
5. You should now see the reply `{“msg”:“Success”}`. If you get an error, it could be that you've waited too long to use the `access_token`. Fetch a new (see previous section) and try again.

### 2.4 Use Postman to automatically fetch an access token on your behalf

Since access tokens are short lived, it becomes a bit of a hassle to manually fetch, copy and paste a new access_token every time and old one expires. Luckily, since Client Credentials are part of the OAuth2 standard, Postman has support for helping with that.

1. Open Postman
2. Open the “Demo test” request
3. Go to the Authorization tab
4. Click the link after where it says "This request is using an authorization helper from collection"
5. Click the “Get New Access Token” button
6. In the Token Name field, enter “Naviga ID Access Token”
7. Change the “Grant Type” dropdown value to “Client Credentials”
8. Set the “Access Token URL to `https://access-token.{{env}}.imid.infomaker.io/v1/token`
9. In the Client ID field, enter `{{client_id}}`
10. In the Client Sercret field, enter `{{client_secret}}`
11. Leave the scope field empty
12. In the “Client Authentication” dropdown, Choose “Send client credentials in body”
13. Click “Request Token”
14. When the token is displayed, click “Use Token” followed by "Update". If you get an error here, please go back and check you've entered the correct values starting in step 5.
15. Click “Send” to perform the request to the demo service
16. You should now see the reply `{“msg”:“Success”}`. If you get an error here, it could be that you waited for too long to use the access_token. Perform this section one more time by starting at step 1.

**Note!** Postman will not automatically request a new access_token when the current one expires, but all you have to do is go to “Authorization”, click the same link, click “Get New Access Token”, click “Request Token” and click “Use Token”.

Congratulations, you are now ready to start playing with real Naviga Content services!